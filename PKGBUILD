# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Ray Rashif <schiv@archlinux.org>
# Mateusz Herych <heniekk@gmail.com>
# Charles Lindsay <charles@chaoslizard.org>

_linuxprefix=linux-zen
_extramodules=extramodules-6.2-zen
pkgname=$_linuxprefix-vhba-module
_pkgname=vhba-module
_pkgver=20211218
pkgver=20211218_6.2.10.zen1_1
pkgrel=1
pkgdesc="Kernel module that emulates SCSI devices"
arch=('x86_64')
url="https://cdemu.sourceforge.net/"
license=('GPL')
makedepends=("$_linuxprefix" "$_linuxprefix-headers")
provides=("$_pkgname=$_pkgver" "VHBA-MODULE")
groups=("$_linuxprefix-extramodules")
install=$_pkgname.install
source=("http://downloads.sourceforge.net/cdemu/$_pkgname-$_pkgver.tar.xz"
        '60-vhba.rules')
sha256sums=('72c5a8c1c452805e4cef8cafefcecc2d25ce197ae4c67383082802e5adcd77b6'
            '3052cb1cadbdf4bfb0b588bb8ed80691940d8dd63dc5502943d597eaf9f40c3b')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
  cd "$srcdir/$_pkgname-$_pkgver"
}

build() {
  _kernver="$(cat /usr/lib/modules/$_extramodules/version)"

  cd "$srcdir/$_pkgname-$_pkgver"
  make -j1 KDIR=/usr/lib/modules/${_kernver}/build
}

package() {
  _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
  depends=("${_linuxprefix}=${_ver}")

  cd "$srcdir/$_pkgname-$_pkgver"
  install -D vhba.ko "$pkgdir/usr/lib/modules/$_extramodules/vhba.ko"

  sed -i "s/EXTRAMODULES=.*/EXTRAMODULES=$_extramodules/" \
    "$startdir/vhba-module.install"

  find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;

  install -Dm644 "$srcdir/60-vhba.rules" \
    "$pkgdir/usr/lib/udev/rules.d/60-$_linuxprefix-vhba.rules"
}
